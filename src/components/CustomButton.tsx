import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { memo } from 'react'
import { vh, vw } from '../utils/dimensions';
import colors from '../utils/colors';

interface Props {
    title: string;
    onPressCustomButton: any;
    extraStyle?: any;
    isDarkMode: boolean;
}
export const CustomButton = memo((props: Props) => {
    return (
        <TouchableOpacity style={[styles.btn, props?.extraStyle, props?.isDarkMode ? { backgroundColor: colors?.WHITE } : undefined]} onPress={props?.onPressCustomButton}>
            <Text style={[styles.btnText, props?.isDarkMode ? { color: colors?.BLACK } : undefined]}>{props?.title}</Text>
        </TouchableOpacity>
    )
})

const styles = StyleSheet.create({
    btn: {
        borderRadius: vw(40),
        backgroundColor: colors.CUSTOM_RED,
        paddingVertical: vh(16),
        paddingHorizontal: vw(38),
        alignSelf: 'center'
    },
    btnText: {
        fontSize: vw(15),
        fontWeight: '600',
        color: colors.WHITE,
        textTransform: 'capitalize',
        textAlign: 'center',
    },
    icon: {
        width: vw(24),
        height: vw(24),
        marginRight: vw(8.2),
    }
})