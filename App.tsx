import { FlatList, Image, LogBox, PermissionsAndroid, SafeAreaView, ScrollView, StyleSheet, Text, View, ActivityIndicator, Modal, TouchableOpacity, Switch, ToastAndroid, Platform } from 'react-native'
import React, { useCallback, useState } from 'react'
import ImagePicker from 'react-native-image-crop-picker';
import axios from 'axios';
import { CustomButton } from './src/components/CustomButton';
import { screenHeight, screenWidth, vh, vw } from './src/utils/dimensions';
import colors from './src/utils/colors';

export default function App() {
  LogBox.ignoreAllLogs(true)
  LogBox.ignoreLogs(['Remote debugger']);

  const [selectedImage, setSelectedImage] = useState<any>({ path: '', exifData: [], width: null, height: null })
  const [isLoading, setIsLoading] = useState<boolean>(false)
  const [isDarkMode, setIsDarkMode] = useState<boolean>(false)

  const uploadImage = (image: any) => {
    setIsLoading(true)
    const formData = new FormData();
    formData?.append('file', { name: 'file_name', type: image?.mime, uri: image?.path })
    formData?.append('upload_preset', 'navdeep_preset')
    formData?.append('cloud_name', 'duiegf7k6')
    formData?.append('api_key', '959836926666625')
    axios.post('https://api.cloudinary.com/v1_1/duiegf7k6/auto/upload', formData, { headers: { 'Content-Type': 'multipart/form-data' } })?.then(
      (response: any) => {
        console.log('response of upload', response)
        let temp: any = image?.exif ?? {}, arr = [{ name: 'Property', value: 'Value' }]
        for (const key in temp) {
          if (temp.hasOwnProperty(key))
            arr?.push({ name: key, value: temp[key] })
        }
        setSelectedImage({ path: response?.data?.url ?? '', exifData: arr ?? [], width: response?.data?.width, height: response?.data?.height })
      },
      (error: any) => {
        setIsLoading(false)
        console.log('error of upload', error);
        if (Platform.OS === 'android')
          ToastAndroid.show(`Image size should not exceed 10 MB`, ToastAndroid.SHORT)
      })
  }

  const onPressChooseImage = async () => {
    const granted = await PermissionsAndroid.requestMultiple([PermissionsAndroid.PERMISSIONS.CAMERA, PermissionsAndroid.PERMISSIONS.READ_MEDIA_IMAGES])
    if (granted['android.permission.CAMERA'] && granted['android.permission.READ_MEDIA_IMAGES']) {
      ImagePicker.openPicker({ includeExif: true, mediaType: 'photo', compressImageQuality: 0.7 })?.then
        ((image: any) => uploadImage(image),
          (error: any) => {
            console.log('error while opening picker', error);
          })
    }
  }

  const onPressEdit = () => {
    ImagePicker.openCropper({ path: selectedImage?.path, includeExif: true, mediaType: 'photo', cropperToolbarTitle: 'Edit your image', freeStyleCropEnabled: true, compressImageQuality: 0.8 })?.then
      ((image: any) => uploadImage(image),
        (error: any) => {
          console.log('error while opening picker', error);
        })
  }

  const onPressFlip = (direction: string) => {
    setIsLoading(true)
    if (selectedImage?.path?.includes(direction)) {
      setSelectedImage((previousValue: any) => ({ ...previousValue, path: previousValue?.path?.replace(direction, '') }))
    }
    else {
      let index = selectedImage?.path?.indexOf('/upload') + 7
      setSelectedImage((previousValue: any) => ({ ...previousValue, path: previousValue?.path?.slice(0, index) + direction + previousValue?.path?.slice(index) }))
    }
  }

  const renderItem = useCallback(({ item, index }: any) => {
    return (
      <View style={index === 0 ? { ...styles.dataRow, backgroundColor: isDarkMode ? colors?.WHITE : colors?.CUSTOM_RED, borderTopLeftRadius: vw(12), borderTopRightRadius: vw(12) } : styles.dataRow}>
        <Text style={index === 0 ? { ...styles.dataCell, color: isDarkMode ? colors?.BLACK : colors?.WHITE } : isDarkMode ? { ...styles.dataCell, color: colors?.WHITE } : styles.dataCell}>{item?.name ?? '-'}</Text>
        <Text style={index === 0 ? { ...styles.dataCell, color: isDarkMode ? colors?.BLACK : colors?.WHITE } : isDarkMode ? { ...styles.dataCell, color: colors?.WHITE } : styles.dataCell}>{item?.value ?? '-'}</Text>
      </View>
    )
  }, [isDarkMode])

  const keyExtractor = useCallback((_: any, index: number) => index.toString(), [])

  const onLoadEnd = () => setIsLoading(false)

  return (
    <SafeAreaView style={isDarkMode ? { ...styles?.container, backgroundColor: '#262626' } : styles?.container}>
      <View style={styles?.header}>
        <Text style={isDarkMode ? { ...styles?.headerText, color: colors?.WHITE } : styles?.headerText}>Image Editor</Text>
        <Switch
          trackColor={{ true: '#767577', false: '#81b0ff' }}
          thumbColor={!isDarkMode ? '#f5dd4b' : 'darkblue'}
          onValueChange={() => {
            setIsDarkMode((previousState: boolean) => {
              if (Platform.OS === 'android')
                ToastAndroid.show(`${!previousState ? 'Dark' : 'Light'} Mode Applied`, ToastAndroid.SHORT)
              return !previousState
            })
          }}
          value={isDarkMode}
          style={{ transform: [{ scaleX: 1.25 }, { scaleY: 1.25 }] }}
        />
      </View>
      <ScrollView contentContainerStyle={{ flexGrow: 1, paddingBottom: vh(50) }} showsVerticalScrollIndicator={false}>
        {
          selectedImage?.path ?
            <>
              <CustomButton title={`choose ${selectedImage?.path ? 'another' : ''} image`} onPressCustomButton={onPressChooseImage} isDarkMode={isDarkMode} />
              <Image source={{ uri: selectedImage?.path }} style={[isDarkMode ? { ...styles?.img, borderColor: colors?.WHITE } : styles?.img, { width: '100%', height: undefined, aspectRatio: 1 }]} onLoadEnd={onLoadEnd} resizeMode='stretch' resizeMethod='scale' />
              <CustomButton title={`crop / rotate`} onPressCustomButton={onPressEdit} isDarkMode={isDarkMode} />
              <View style={styles?.btnGroup}>
                <CustomButton title={`flip\nvertical`} onPressCustomButton={() => onPressFlip('/a_vflip')} isDarkMode={isDarkMode} />
                <CustomButton title={`flip\nhorizontal`} onPressCustomButton={() => onPressFlip('/a_hflip')} isDarkMode={isDarkMode} />
              </View>
              <FlatList
                data={selectedImage?.exifData}
                keyExtractor={keyExtractor}
                renderItem={renderItem}
                ItemSeparatorComponent={() => <View style={{ height: vw(1), width: '100%', backgroundColor: isDarkMode ? colors?.WHITE : colors?.CUSTOM_RED }} />}
                contentContainerStyle={{ borderWidth: vw(1), borderColor: isDarkMode ? colors?.WHITE : colors?.CUSTOM_RED, borderRadius: vw(12) }}
                nestedScrollEnabled
                showsVerticalScrollIndicator={false}
              />
            </>
            :
            <View style={styles?.emptyImageView}>
              <CustomButton title={`choose ${selectedImage?.path ? 'another' : ''} image`} onPressCustomButton={onPressChooseImage} isDarkMode={isDarkMode} />
            </View>
        }
      </ScrollView>
      <Modal transparent={true} animationType='fade' visible={isLoading}>
        <TouchableOpacity onPress={onLoadEnd} style={styles.modalBackground} activeOpacity={1}>
          <ActivityIndicator animating={isLoading} color={isDarkMode ? colors?.WHITE : colors?.CUSTOM_RED} size={90} />
        </TouchableOpacity>
      </Modal>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: vw(20),
    backgroundColor: colors?.WHITE,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: vh(20),
  },
  headerText: {
    fontSize: vw(25),
    fontWeight: '800',
    color: colors.CUSTOM_RED,
  },
  img: {
    borderWidth: vw(2),
    borderRadius: vw(12),
    borderColor: colors?.CUSTOM_RED,
    marginVertical: vh(20),
    alignSelf: 'center'
  },
  btnGroup: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    margin: vw(20),
  },
  dataRow: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: vh(10)
  },
  dataCell: {
    width: '50%',
    textAlign: 'center',
    fontSize: vw(14),
    fontWeight: '800',
    color: colors.CUSTOM_RED,
  },
  emptyImageView: {
    width: '100%',
    height: vh(300),
    borderWidth: vw(2),
    borderRadius: vw(12),
    borderStyle: 'dashed',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: 'gray',
    marginTop: vh(100)
  },
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: 'rgba(0,0,0,0.6)',
  },
})